package it.polimi.hegira.cli;

import it.polimi.hegira.utils.SupportedDatabases;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

/**
 * @author Marco Scavuzzo
 *
 */
public class DatabaseValidator implements IParameterValidator {

	
	@Override
	public void validate(String name, String value) throws ParameterException {
		SupportedDatabases sd = SupportedDatabases.getInstance();
		String mValue = value.toUpperCase();
		if(!sd.getSupportedDatabases().contains(mValue) ||
			!sd.getAlternativeNames().contains(mValue)){
			throw new ParameterException("The given database is not supported");
		}
	}

}
