package it.polimi.hegira.cli;

import java.util.List;

import com.beust.jcommander.Parameter;

/**
 * @author Marco Scavuzzo
 *
 */
public class CLI {
	
	@Parameter(names = {"--database","-d"},
			required = true,
			description = "The database that will be used to check data consistency "
					+ "(e.g., GAE_DATASTORE)")
	public String database;
	
	@Parameter(names = { "--credentials","-c" }, 
			description = "Credentials file. Otherwise default path will be used")
	public String credentials;
	
	@Parameter(names = {"--filters", "f"},
			variableArity=true,
			description = "White list of all the tables/kinds/etc.. that will be "
					+ "considered during data extraction. If not specified, "
					+ "all the database will be extracted.")
	public List<String> filters;
	
	@Parameter(names = "--help", 
			help = true,
			description = "Prints this help message")
	private boolean help;
}
