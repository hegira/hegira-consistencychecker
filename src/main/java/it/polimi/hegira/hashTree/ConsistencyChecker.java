package it.polimi.hegira.hashTree;

import java.io.File;
import java.io.IOException;

import org.hashtrees.HashTrees;
import org.hashtrees.HashTreesImpl;
import org.hashtrees.SimpleTreeIdProvider;
import org.hashtrees.SyncDiffResult;
import org.hashtrees.SyncType;
import org.hashtrees.store.HashTreesPersistentStore;
import org.hashtrees.store.SimpleMemStore;
import org.hashtrees.store.Store;
import org.hashtrees.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marco Scavuzzo
 *
 */
public class ConsistencyChecker {
	private transient Logger log = LoggerFactory.getLogger(ConsistencyChecker.class);
	private static ConsistencyChecker instance = null;
	private static Boolean instance_lock = true;
	private Store primaryStore, backupStore;
	private HashTreesImpl primaryHTI, backupHTI;
	
	private ConsistencyChecker(){
		try {
			Pair<Store, HashTreesImpl> primary = createAStoreWithEnabledHashTrees("primary");
			Pair<Store, HashTreesImpl> backup = createAStoreWithEnabledHashTrees("backup");
			start(primary.getSecond(), backup.getSecond());
			this.primaryStore = primary.getFirst();
			this.primaryHTI = primary.getSecond();
			this.backupStore = backup.getFirst();
			this.backupHTI = backup.getSecond();
		} catch (IOException e) {
			log.error("{} - Problem with the HashTree database. Stack Trace:\n",
					Thread.currentThread().getName(),
					e);
		}
		
	}
	
	public static ConsistencyChecker getInstance(){
		synchronized(instance_lock){
			instance = instance==null ? new ConsistencyChecker() : instance;
		}
		return instance;
	}
	
	public void checkConsistency() throws IOException{
		SyncDiffResult diffResult = primaryHTI.synch(1, backupHTI, SyncType.FIND_DIFF_ONLY);
		
		System.out.println("Consistency check. Result:\n"+diffResult+"\n");
	}
	
	/**
	 * As a first step, hashtree requires a store object. On store object,
	 * hashtrees will perform read/write operations.(For example during synch of
	 * primary and secondary). You need to implement {@link Store} interface.
	 * 
	 * The following function just implements a in memory version of
	 * {@link Store} for this example.
	 * 
	 * @return
	 */
	private static Store buildStore() {
		return new SimpleMemStore();
	}

	/**
	 * Once you have the {@link Store} object, you need to build the instance of
	 * {@link HashTrees}. {@link HashTreesImpl} is the actual implementation,
	 * and it is also provided with {@link HashTreesImpl.Builder} to easily
	 * build the instance of {@link HashTreesImpl}.
	 * 
	 * Following example creates the hash trees with in memory store
	 * implementation.
	 * 
	 * @param store
	 * @param dbName 
	 * @return
	 * @throws IOException
	 */
	private static HashTreesImpl buildHashTrees(Store store, String dbName) throws IOException {
		String path = System.getProperty("user.home")+File.separator+
				"merkleTrees"+File.separator+
				dbName+".ldb";
		
		HashTreesImpl hashTrees = new HashTreesImpl.Builder(store,
				new SimpleTreeIdProvider(), new HashTreesPersistentStore(path))
				.setEnabledNonBlockingCalls(false).setNoOfSegments(16).build();
		return hashTrees;
	}

	/**
	 * We need to register {@link HashTrees} with {@link Store} object. So that
	 * {@link Store} will forward the necessary calls to {@link HashTrees}. Note
	 * it is not compulsory to implement registerHashTrees method. As long as
	 * the {@link HashTrees} gets update operations in some way, its more than
	 * enough.
	 * 
	 * @param store
	 * @param hashTrees
	 */
	private static void registerHashTreesWithStore(Store store,
			HashTreesImpl hashTrees) {
		store.registerHashTrees(hashTrees);
	}

	/**
	 * The following function creates a {@link Store} and {@link HashTrees}
	 * object to support the {@link Store}.
	 * @param dbName 
	 * 
	 * @return
	 * @throws IOException
	 */
	private static Pair<Store, HashTreesImpl> createAStoreWithEnabledHashTrees(String dbName)
			throws IOException {
		Store store = buildStore();
		HashTreesImpl hashTrees = buildHashTrees(store, dbName);
		registerHashTreesWithStore(store, hashTrees);
		return Pair.create(store, hashTrees);
	}

	/**
	 * {@link HashTreesImpl} requires explicitly calling its
	 * {@link HashTreesImpl#start()} method.
	 * 
	 * @param hashTrees
	 */
	private static void start(HashTreesImpl... hashTrees) {
		for (HashTreesImpl hashTree : hashTrees)
			hashTree.start();
	}

	public void stop(){
		stop(primaryHTI, backupHTI);
	}
	
	/**
	 * {@link HashTreesImpl} requires explicitly calling its
	 * {@link HashTreesImpl#stop()} method.
	 * 
	 * @param hashTrees
	 */
	private static void stop(HashTreesImpl... hashTrees) {
		if(instance==null)
			return;
		for (HashTreesImpl hashTree : hashTrees)
			hashTree.stop();
	}

	/**
	 * @return the primaryStore
	 */
	public synchronized Store getPrimaryStore() {
		return primaryStore;
	}

	/**
	 * @return the backupStore
	 */
	public synchronized Store getBackupStore() {
		return backupStore;
	}
	
}
