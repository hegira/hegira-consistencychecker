package it.polimi.hegira.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class to retrieve data from properties files.
 * @author Marco Scavuzzo
 *
 */
public class PropertiesManager {
	private transient static Logger log = LoggerFactory.getLogger(PropertiesManager.class);
	private String defaultPath = System.getProperty("user.home")+File.separator+
			"hegira"+File.separator+
			"consistency_checker"+File.separator;
	private String credentialsFileName = "credentials.properties";
	private File credentialsFile = null;
	private static PropertiesManager instance = new PropertiesManager();
	
	private PropertiesManager(){
		try {
			this.credentialsFile = createFileIfNotExists(defaultPath, credentialsFileName);
		} catch (NullPointerException | IOException e) {
			log.error("{} - Error creating file {}.\nStack Trace:",
					Thread.currentThread().getName(),
					defaultPath+credentialsFileName,
					e);
		}
	}
	
	public static PropertiesManager getInstance(){
		return instance;
	}
	
	private File createFileIfNotExists(String path, String fileName) 
			throws NullPointerException, IOException{
		if(fileName == null)
			throw new NullPointerException("fileName cannot be null");
		if(path==null)
			path=defaultPath;
		
		File file = new File(path+fileName);
		if (!file.getParentFile().exists())
		    file.getParentFile().mkdirs();
		if (!file.exists())
		    file.createNewFile();
		return file;
	}
	
	public void setCredentialsFile(String filePath) throws FileNotFoundException{
		if(filePath == null)
			throw new NullPointerException("filePath cannot be null");
		this.credentialsFile = new File(filePath);
		if (!this.credentialsFile.exists())
			throw new FileNotFoundException(filePath+" doesn't exist!");
	}
	
	/**
	 * Gets the value of a given property stored inside the credentials file.
	 * @param property	The name of the property.
	 * @return	The value for the given property name.
	 */
	public String getCredentialsProperty(String property){
		return getProperty(credentialsFile, property);
	}
	
	public Map<String,String> getCredentialProperties(String...properties){
		return getProperties(credentialsFile, properties);
	}
	
	public String getDefaultPath(){
		return defaultPath;
	}
	
	/**
	 * Gets the value of a given property stored inside the credentials file.
	 * @param property	The name of the property.
	 * @return	The value for the given property name.
	 */
	public String getProperty(File file, String property){
		Properties props = new Properties();
		InputStream isr = null;
		try {
			isr = new FileInputStream(file);	
			props.load(isr);
			return props.getProperty(property);
		} catch (IOException e) {
			log.error("{} - Error reading file {}.\nStack Trace:",
					Thread.currentThread().getName(),
					file != null ? file : "",
					e);
		} catch (Exception e) {
			log.error("{} - Error reading file {}.\nStack Trace:",
					Thread.currentThread().getName(),
					file != null ? file : "",
					e);
		} finally {
			try {
				if(isr!=null)
					isr.close();
			} catch (IOException e) {}
			props=null;
		}
		return null;
	}
	
	public Map<String,String> getProperties(File file, String...propertiesNames){
		Properties props = new Properties();
		InputStream isr = null;
		try {
			isr = new FileInputStream(file);	
			props.load(isr);
			HashMap<String,String> map = new HashMap<String,String>();
			for(String pName : propertiesNames){
				String prop = props.getProperty(pName);
				if(prop!=null)
					map.put(pName, prop);
			}
			return map;
		} catch (IOException e) {
			log.error("{} - Error reading file {}.\nStack Trace:",
					Thread.currentThread().getName(),
					file != null ? file : "",
					e);
		} catch (Exception e) {
			log.error("{} - Error reading file {}.\nStack Trace:",
					Thread.currentThread().getName(),
					file != null ? file : "",
					e);
		} finally {
			try {
				if(isr != null)
					isr.close();
			} catch (IOException e) {}
			props=null;
		}
		
		return null;
	}
}
