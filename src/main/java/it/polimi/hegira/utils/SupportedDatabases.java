package it.polimi.hegira.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * @author Marco Scavuzzo
 *
 */
public class SupportedDatabases {
	private static SupportedDatabases instance;
	private List<String> supportedDatabases;
	//K = alternative name; V = proper name 
	private HashMap<String,String> alternativeNames;
	
	private SupportedDatabases(){
		this.supportedDatabases = new ArrayList<String>();
		this.alternativeNames = new HashMap<String,String>();
		populate();
	}
	
	public static SupportedDatabases getInstance(){
		return instance == null ? new SupportedDatabases() : instance;
	}
	
	//ADD NEW DATABASES HERE!
	private void populate(){
		addDatabase("GAE_DATASTORE","DATASTORE");
	}
	
	private void addDatabase(String properName, String...altNames){
		if(properName==null) return;
		DatabaseNames db = new DatabaseNames(properName, altNames);
		supportedDatabases.add(db.properName);
		for(String alt : db.alternativeNames){
			alternativeNames.put(alt, db.properName);
		}
	}
	
	public List<String> getSupportedDatabases(){
		return supportedDatabases;
	}
	
	public Set<String> getAlternativeNames(){
		return alternativeNames.keySet();
	}
	
	/**
	 * Given an alternative or proper database name, returns its associated proper name.
	 * @param altName
	 * @return
	 */
	public String getProperName(String altName){
		return supportedDatabases.contains(altName) ? altName : alternativeNames.get(altName);
	}
	
	private class DatabaseNames{
		private String properName;
		private List<String> alternativeNames;
		
		public DatabaseNames(String properName, String...altNames){
			this.properName = properName;
			this.alternativeNames = new ArrayList<String>(Arrays.asList(altNames));
		}
	}
}
