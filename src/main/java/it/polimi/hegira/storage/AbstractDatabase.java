package it.polimi.hegira.storage;

import java.util.ArrayList;
import java.util.List;

import it.polimi.hegira.exceptions.ConnectException;
import it.polimi.hegira.hashTree.ConsistencyChecker;

import org.hashtrees.store.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractDatabase {
	private transient Logger log = LoggerFactory.getLogger(AbstractDatabase.class);
	private StorageType storageType;
	protected Store hashTreeStore;
	protected List<String> filters;
	
	public AbstractDatabase(StorageType type){
		this.storageType = type;
		switch(type){
			case primary:
				hashTreeStore = ConsistencyChecker.getInstance().getPrimaryStore();
				break;
			case secondary:
				hashTreeStore = ConsistencyChecker.getInstance().getBackupStore();
				break;
		}
		filters = new ArrayList<String>(1);
	}
	
	/**
	 * Gives the possibility to simply filter (white-listing) the tables/kinds,etc... specified in the list.
	 * I.e., if specified, only the tables/kinds,etc... in the list are extracted;
	 * otherwise all the database is extracted.
	 * @param filtersList
	 */
	public void setExtractionFilter(List<String> filtersList){
		if(filtersList!=null){
			this.filters = filtersList;
		}
	}
	
	/**
	* Encapsulate the logic contained inside the models to map a DB entities to the intermediate
	* model entities and build the relative Hash Tree
	* @param model The model to be converted
	*/
	protected abstract void toMyModel(AbstractDatabase model);
	/**
	 * Start a connection towards the database. 
	 * Suggestion: To be called inside a try-finally block. Should always be disconnected
	 * @throws ConnectException
	 */
	public abstract void connect(StorageType type) throws ConnectException;
	/**
	 * Disconnects and closes all connections to the database.
	* Suggestion: To be called inside a finally block
	*/
	public abstract void disconnect();
	
	/**
	 * Template method which builds the Merkle Tree
	 */
	public final Thread buildHashTree(){
		final AbstractDatabase thiz = this;
		Thread buildingThread;
		buildingThread = new Thread() {
			@Override public void run() {		
				try {
					thiz.connect(storageType);
					thiz.toMyModel(thiz);
				} catch (ConnectException e) {
					log.error("{} - Error connecting to the database.\n Stack trace: ",
							Thread.currentThread().getName(),
							e);
				} finally{
					thiz.disconnect();
				}     
			}
		};
		
		buildingThread.start();
		return buildingThread;
	}
	
	/**
	 * Returns a list containing all the tables of the database.
	 * @return The list of tables.
	 */
	public abstract List<String> getTableList();
	
	public enum StorageType {
		primary, secondary
	}
}
