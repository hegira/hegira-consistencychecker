package it.polimi.hegira.storage;

import it.polimi.hegira.storage.AbstractDatabase.StorageType;

/**
 * Applying the Factory pattern to create proper database objects
 * @author Marco Scavuzzo
 *
 */
public class DatabaseFactory {
	/**
	* Get an instance of a database as a producer or as a consumer
	* @param name Name of the database to be instantiated
	* @param type The type of database: primary or secondary.
	* @return
	*/
	public static AbstractDatabase getDatabase(String name, StorageType type){
		switch(name){
			case "GAE_DATASTORE":
				return new Datastore(type);
			case "AZURE_TABLES":
				return null;
			case "CASSANDRA":
				return null;
			case "HBASE":
				return null;
			default:
				return null;
		}
	}
}
