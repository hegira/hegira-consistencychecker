package it.polimi.hegira.storage;

import it.polimi.hegira.storage.AbstractDatabase;
import it.polimi.hegira.exceptions.ConnectException;
import it.polimi.hegira.hegira_metamodel.Metamodel;
import it.polimi.hegira.transformers.DatastoreTransformer;
import it.polimi.hegira.utils.PropertiesManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.thrift.TException;
import org.apache.thrift.TSerializer;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entities;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.KeyRange;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.QueryResultList;
import com.google.appengine.tools.remoteapi.RemoteApiInstaller;
import com.google.appengine.tools.remoteapi.RemoteApiOptions;

public class Datastore extends AbstractDatabase {
	private static Logger log = LoggerFactory.getLogger(Datastore.class);
	private ArrayList<ConnectionObject> connectionList;
	
	private class ConnectionObject{
		public ConnectionObject(){}
		public ConnectionObject(RemoteApiInstaller installer, DatastoreService ds){
			this.installer = installer;
			this.ds = ds;
		}
		protected RemoteApiInstaller installer;
		protected DatastoreService ds;
	}
	
	public Datastore(StorageType type) {
		super(type);
		connectionList = new ArrayList<ConnectionObject>(1);
		connectionList.add(new ConnectionObject());
	}

	@Override
	protected void toMyModel(AbstractDatabase db) {
		Datastore datastore = (Datastore) db;
		List<String> kinds = datastore.getAllKinds();
		
		//if specified, lets apply the white-listing
		if(filters!=null && !filters.isEmpty()){
			kinds.retainAll(filters);
		}
		
		//Create a new instance of the Thrift Serializer
        TSerializer serializer = new TSerializer(new TBinaryProtocol.Factory());
        
		for(String kind : kinds){
			long i=0;
	        //Datastore cursor to scan a kind
	        Cursor cursor = null;
	        
	        while(true){
	        		QueryResultList<Entity> results = datastore.getEntitiesByKind_withCursor(kind, cursor, 300);
	        		
	        		//CURSOR CODE
	        		Cursor newcursor = getNextCursor(results);
				/**
				 * newcursor is null if the query result cannot be resumed;
				 * newcursor is equal to cursor if all entities have been read.
				 */
				if(newcursor==null || newcursor.equals(cursor)) break;
				else cursor = newcursor;
				
				//PRODUCTION CODE
				for(Entity entity : results){
					DatastoreModel dsModel = new DatastoreModel(entity);
					dsModel.setAncestorString(entity.getKey().toString());
					DatastoreTransformer dt = new DatastoreTransformer();
					Metamodel myModel = dt.toMyModel(dsModel);
					
					if(myModel!=null){
						try {
							//Populating the HashTree
							byte[] serializedEntity = serializer.serialize(myModel);
							hashTreeStore.put(myModel.rowKey.getBytes(),
									serializedEntity);
							i++;
						} catch (TException e) {
							log.error("{} - Serialization Error: ", 
									Thread.currentThread().getName(),
									e);
						} catch (IOException e) {
							log.error("{} - Cannot put data in the HashTree database!! Stack Trace:\n",
									Thread.currentThread().getName(),
									e);
						}
					}
				}
	        }
	        log.debug(Thread.currentThread().getName()+" ==> Transferred "+i+" entities of kind "+kind);
		}
	}
	
	/**
	 * Returns the next cursor relative to the given list of entities.
	 * @param results The list of entities.
	 * @return	The next cursor. <code>null</code> if the query result cannot be resumed;
	 */
	private Cursor getNextCursor(QueryResultList<Entity> results){
		//trying to minimize undocumented errors from the Datastore
		boolean proofCursor = true;
		int timeout_ms=100, retries=10;
		
		Cursor newcursor=null;
		while(proofCursor && retries>0){
			try{
				newcursor = results.getCursor();
				proofCursor = false;
			}catch(Exception e){
				log.error("\n\n\n\n\t\tUndocumented Error !!! "+e.getMessage()+"\n\n\n");
				try {
					Thread.sleep(timeout_ms);
					if(timeout_ms<5000) timeout_ms*=2;
				} catch (InterruptedException e1){
					proofCursor=false;
				}
				retries--;
			}
		}
		return newcursor;
	}

	@Override
	public void connect(StorageType type) throws ConnectException {
		int thread_id = 0;
		if(!isConnected()){
			PropertiesManager pm = PropertiesManager.getInstance();
			Map<String, String> credentialProperties = pm.getCredentialProperties(
					"datastore."+type.name()+".username",
					"datastore."+type.name()+".password",
					"datastore."+type.name()+".server");
			String username = credentialProperties.get("datastore."+type.name()+".username");
			String password = credentialProperties.get("datastore."+type.name()+".password");
			String server = credentialProperties.get("datastore."+type.name()+".server");
			
			RemoteApiOptions options = new RemoteApiOptions()
        			.server(server, 443)
        			.credentials(username, password);
			
			try {
				log.debug(Thread.currentThread().getName()+" - Logging into "+server);
				RemoteApiInstaller installer = new RemoteApiInstaller();
				installer.install(options);
				DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
				ConnectionObject co = new ConnectionObject(installer, ds);
				connectionList.add(thread_id,co);
				log.debug(Thread.currentThread().getName()+" - Added connection object at "+
				"position: "+connectionList.indexOf(co)+
				" ThreadId%THREAD_NO="+thread_id);
			} catch (IOException e) {
				log.error("{} - Cannot connect to {} storage. Stack Trace:\n",
						Thread.currentThread().getName(),
						type.name(),
						e);
				throw new ConnectException("Cannot connect to storage");
			}
		}else{
			log.warn("{} - {} storage already connected.",
					Thread.currentThread().getName(),
					type.name());
		}
	}

	/**
	 * Checks if a connection has already been established
	 * @return true if connected, false if not.
	 */
	public boolean isConnected(){
		int thread_id = 0;
		try{
			return (connectionList.get(thread_id).installer==null || 
					connectionList.get(thread_id).ds==null) ? false : true;
		}catch(IndexOutOfBoundsException e){
			return false;
		}
	}
	
	@Override
	public void disconnect() {
		int thread_id = 0;
		if(isConnected()){
			if(connectionList.get(thread_id).installer!=null)
				connectionList.get(thread_id).installer.uninstall();
			connectionList.get(thread_id).installer = null;
			connectionList.get(thread_id).ds = null;
			log.debug(Thread.currentThread().getName() + " Disconnected");
		}else{
			log.warn("{} - storage not connected.",
					Thread.currentThread().getName());
		}
	}
	
	
	private Map<Key,Entity> getEntitiesByKeys(List<Integer> keys, String kind){
		int thread_id = 0;
		//building Datastore keys
		ArrayList<Key> dKeys = new ArrayList<Key>(keys.size());
		for(Integer ik : keys){
			Key dk = KeyFactory.createKey(kind, ik.toString());
			Key dkl = KeyFactory.createKey(kind, ik.longValue());
			dKeys.add(dk);
			dKeys.add(dkl);
		}
		//querying for the given keys
		return connectionList.get(thread_id).ds.get(dKeys);
	}
	
	/**
	 * Uses Datastore SDK's KeyRange class to efficiently get a range of entities.
	 * @param start The id of the first entity in the range.
	 * @param end The id of the last entity in the range.
	 * @param kind The kind of the entities to retrieve.
	 * @return The query result, i.e. the entities associated with the KeyRange.
	 */
	private Map<Key,Entity> getEntitiesByKeyRange(long start, long end, String kind){
		int thread_id = 0;
		//building Datastore keys
		KeyRange dKeys = new KeyRange(null, kind, start, end);
		//querying for the given keys
		return connectionList.get(thread_id).ds.get(dKeys);
	}
	
	/**
    * Query for a given entity type
    * @param ds The datastore object to connect to the actual datastore
    * @param kind The kind used for the retrieval
    * @return An iterable containing all the entities
    */
   private Iterable<Entity> getEntitiesByKind(String kind){
	   int thread_id = 0;
   		Query q = new Query(kind);
   		PreparedQuery pq = connectionList.get(thread_id).ds.prepare(q);
   		return pq.asIterable();
   }
   
   /**
    * Gets a batch of entities of a given kind
    * @param kind The Entity Kind 
    * @param cursor The point where to start fetching entities (<code>null</code> if entities should be fetched). Could be extracted from the returned object.
    * @param pageSize The number of entities to be retrieved in each batch (maximum 300).
    * @return An object containing the entities, the cursor and other stuff.
    */
   private QueryResultList<Entity> getEntitiesByKind_withCursor(String kind, 
   		Cursor cursor, int pageSize){
	   int thread_id = 0;
   		boolean proof = true;
   		QueryResultList<Entity> results = null;
	   	/**
	   	 * Bullet proof reads from the Datastore.
	   	 */
	   	while(proof){
	   		try{
		    		FetchOptions fetchOptions = FetchOptions.Builder.withLimit(pageSize);
		    		if(cursor!=null)
		    			fetchOptions.startCursor(cursor);
		        	Query q = new Query(kind);
		        	PreparedQuery pq = connectionList.get(thread_id).ds.prepare(q);
		        	results = pq.asQueryResultList(fetchOptions);
		        	proof = false;
	   		}catch(Exception e){
	   			log.error(Thread.currentThread().getName() + 
	   					"ERROR: getEntitiesByKind_withCursor -> "+e.getMessage());
	   		}
	       	
	   	}
			
	   	return results;
   }
   
   /**
    * Gets all the entities descending (even not directly connected) from the given key
    * @param ds the Datastore object
    * @param ancestorKey get descendents of this key
    * @return
    */
   private Iterable<Entity> getDescentents(Key ancestorKey){
	   int thread_id = 0;
	    	Query q = new Query().setAncestor(ancestorKey);
	    	PreparedQuery pq = connectionList.get(thread_id).ds.prepare(q);
	    	return pq.asIterable();
   }
   
   /**
    * All entities kinds contained in the Datastore, excluding statistic ones.
    * @return  A list containing all the kinds
    */
   public List<String> getAllKinds(){
	   int thread_id = 0;
	   	Iterable<Entity> results = connectionList.get(thread_id).ds.prepare(new Query(Entities.KIND_METADATA_KIND)).asIterable();
	   	//list containing kinds of the root entities
	   	ArrayList<String> kinds = new ArrayList<String>();
	   	for(Entity globalStat : results){
	   		Key key2 = globalStat.getKey();
	   		String name = key2.getName();
		    	if(name.indexOf("_")!=0){
		    		kinds.add(name);
		    	}
	   	}
	   	return kinds;
   }
   
   /**
    * Checks if an entity is root (i.e. it hasn't any parent)
    * @param e The entity to be checked
    * @return <code>true</code> if the given entity is root, <code>false</code> otherwise;
    */
   private boolean isRoot(Entity e){
	   	Key key = e.getKey();
	   	Key parentKey = key.getParent();
	   	return (parentKey==null) ? true : false;
   }

	@Override
	public List<String> getTableList() {
		return getAllKinds();
	}

}
