package it.polimi.hegira.hegira_consistency;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;

import it.polimi.hegira.cli.CLI;
import it.polimi.hegira.hashTree.ConsistencyChecker;
import it.polimi.hegira.storage.AbstractDatabase;
import it.polimi.hegira.storage.AbstractDatabase.StorageType;
import it.polimi.hegira.storage.DatabaseFactory;
import it.polimi.hegira.utils.PropertiesManager;
import it.polimi.hegira.utils.SupportedDatabases;

/**
 * @author Marco Scavuzzo
 *
 */
public class EntryClass {
	private transient static Logger log = LoggerFactory.getLogger(EntryClass.class);
	public static void main(String[] args) {
		/**
		 * 1. Choose the database where to retrieve the two versions of the data
		 * 2. Get the database's credentials with different parameters for the primary and secondary data
		 * 3. Build the Merkle's Trees (possibly in parallel)
		 * 	3.1 Extract all the data (entities) from the database respective account;
		 * 	3.2 For each entity generate the Metamodel entity;
		 * 	3.3 Serialize such entity;
		 * 	3.4 Use the serialized data (byte array) to build the Merkle's Tree 
		 * 		(i.e., calculate the hash function of the Metamodel byte representation).
		 * 4. Check whether the two trees are the same (it would suffice to check just the root nodes)  
		 */
		CLI cli = initCLI(args);
		if(cli == null) return;
		
		handleCredentials(cli);
		
		String database = SupportedDatabases.getInstance().getProperName(cli.database);
		
		AbstractDatabase pDB = DatabaseFactory.getDatabase(database, StorageType.primary);
		AbstractDatabase sDB = DatabaseFactory.getDatabase(database, StorageType.secondary);
		
		if(cli.filters != null && !cli.filters.isEmpty()){
			pDB.setExtractionFilter(cli.filters);
			sDB.setExtractionFilter(cli.filters);
		}
		
		Thread pDBthread = pDB.buildHashTree();
		Thread sDBthread = sDB.buildHashTree();
		
		//wait for Hash Trees population completion
		try {
			pDBthread.join();
			sDBthread.join();
		} catch (InterruptedException e) {
			log.error("",e);
		}
		
		try {
			ConsistencyChecker.getInstance().checkConsistency();
			ConsistencyChecker.getInstance().stop();
		} catch (IOException e) {
			log.error("Couldn't check the consistency between the Hash Trees!");
		}
	}
	
	private static void handleCredentials(CLI cli){
		if(cli.credentials != null && !cli.credentials.equals("")){
			try {
				PropertiesManager.getInstance().setCredentialsFile(cli.credentials);
			} catch (FileNotFoundException e) {
				log.warn("Wrong file path or file not found. "
						+ "Considering default properties if present ({})",
						PropertiesManager.getInstance().getDefaultPath());
			}
		}
	}
	
	private static CLI initCLI(String[] args){
		CLI cli = new CLI();
		JCommander jc = new JCommander(cli);
		try{
			jc = new JCommander(cli,args);
		}catch(Exception e){
			jc.usage();
			return null;
		}
		return cli;
	}

}
